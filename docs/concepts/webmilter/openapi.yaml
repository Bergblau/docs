openapi: 3.0.3
info:
  title: WebMilter backend service
  description: >-
    This ist the standard backend service for the [WebMilter concept] in
    [Bergblau].

    It is consumed by the [WebMilter MTA daemon], hooked into a Mail Transport
    Agent (MTA) like [Postfix].

      [WebMilter concept]: https://docs.bergblau.io/concepts/webmilter/
      [Bergblau]: https://bergblau.io/
      [WebMilter MTA daemon]: https://docs.bergblau.io/components/webmilterd/
      [Postfix]: https://www.postfix.org/
  contact:
    email: concepts@bergblau.io
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 0.2.0
tags:
  - name: provision
    description: Provisioning and configuration of milter service
  - name: alias
    description: E-mail aliases and recipient routing
  - name: policy
    description: Policy checks for mail routing
  - name: sender
    description: Calls depending on the sender of an e-mail
paths:
  /alias/{address}:
    get:
      operationId: resolveAliasByAddress
      summary: Resolve an alias by its mail address
      tags:
        - alias
      parameters:
        - in: path
          name: address
          schema:
            type: string
          required: true
          description: Full address to be resolved
      responses:
        '200':
          description: List of target addresses of this alias
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Recipients'
  /policy/{address}:
    get:
      operationId: checkPolicyByAddress
      summary: Check policy by the recipient's address
      tags:
        - policy
        - sender
      parameters:
        - in: path
          name: address
          schema:
            type: string
          required: true
          description: Full address of recipient
        - in: query
          name: sender
          schema:
            type: string
          required: false
          description: Address of the sender
      responses:
        '200':
          description: Policy action to take for this mail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PolicyAction'
  /domain/{domain}/alias/{local}:
    get:
      operationId: resolveAliasByDomainAndLocalpart
      summary: Resolve an alias by its local part below a domain
      tags:
        - alias
      parameters:
        - in: path
          name: domain
          schema:
            type: string
          required: true
          description: Mail domain to handle aliases under
        - in: path
          name: local
          schema:
            type: string
          required: true
          description: Local part of alias address
      responses:
        '200':
          description: List of target addresses of this alias
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Recipients'
  /domain/{domain}/policy/{local}:
    get:
      operationId: checkPolicyByDomainAndLocalpart
      summary: Check policy by the recipient's local part below a domain
      tags:
        - policy
        - sender
      parameters:
        - in: path
          name: domain
          schema:
            type: string
          required: true
          description: Mail domain to check policy for
        - in: path
          name: local
          schema:
            type: string
          required: true
          description: Local part of recipient address
        - in: query
          name: sender
          schema:
            type: string
          required: false
          description: Address of the sender
      responses:
        '200':
          description: Policy action to take for this mail
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PolicyAction'
components:
  schemas:
    Recipients:
      type: array
      items:
        type: string
    PolicyAction:
      type: object
      required: [action]
      properties:
        action:
          type: string
          description: The policy action to be taken
          enum: [accept, defer, hold, reject, discard]
        reason:
          type: string
          description: Optional reason for the action to be transmitted back to client
